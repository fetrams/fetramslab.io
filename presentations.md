---
bg: "rev01_b.png"
layout: page
permalink: /slide/
title: "Presentaciones"
crawlertitle: "All presentations"
summary: "Presentaciones Disponibles"
active: presentations
---

<ul class="post-list">
  {% for post in site.posts %}
    {% if post.categories contains "presentation" %}

      <li>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>
          <h2>
            <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title}}</a>
          </h2>
      </li>
    {% else %}
      <!--  only show presentations -->
    {% endif %}
  {% endfor %}
</ul>
