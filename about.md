---
bg: "rev30_b.png"
layout: page
title: "Acerca Fetrams"
crawlertitle: "Why and how this page was created"
permalink: /aboutfetrams/
summary: "Federación Nacional de Transportadores en Motocarros"
active: about
---
# Federación Nacional de Transportadores en Motocarros
![www.fetrams.com.co]({{ site.images }}/fetrams_logo.png)
<a href="https://www.facebook.com/Federaci%C3%B3n-Nacional-de-Transportadores-en-Motocarros-Fetrams-963875656996366">
    <i class="fa fa-facebook-official"></i>
  </a>**FETRAMS** como organización empresarial democrática y participativa se une voluntariamente para hacer frente a las necesidades y aspiraciones socioeconómicas y culturales de los federados; su actividad principal será desarrollar la actividad de la industria del transporte terrestre automotor de pasajeros en vehículos motocarro por intermedio de sus personas jurídicas.

[Más información](mailto:presidenciafetrams@gmail.com)
